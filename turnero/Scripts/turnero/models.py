# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Cliente(models.Model):
    id_cliente = models.BigIntegerField(primary_key=True)
    id_persona = models.ForeignKey('Persona', db_column='id_persona', blank=True, null=True, on_delete=models.CASCADE)
    numero_documento = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'cliente'


class ColaAtencionCabecera(models.Model):
    id_cola_cabecera = models.BigIntegerField(primary_key=True)
    fecha_hora = models.DateField(blank=True, null=True)
    nro_cola = models.BigIntegerField(blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        db_table = 'cola_atencion_cabecera'

class ColaAtencionDetalle(models.Model):
    id_cola_detalle = models.BigIntegerField(primary_key=True)
    id_cola_cabecera = models.ForeignKey(ColaAtencionCabecera, db_column='id_cola_cabecera', on_delete=models.CASCADE)
    id_turno = models.ForeignKey('Turno', db_column='id_turno', blank=True, null=True, on_delete=models.CASCADE)
    nro_ticket = models.BigIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'cola_atencion_detalle'
        unique_together = (('id_cola_detalle', 'id_cola_cabecera'),)


class Empresa(models.Model):
    id_empresa = models.BigIntegerField(primary_key=True)
    razon_social = models.CharField(max_length=200, blank=True, null=True)
    ruc = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'empresa'


class Parametro(models.Model):
    id_parametro = models.BigIntegerField(primary_key=True)
    id_usuario_insercion = models.ForeignKey('Usuario', db_column='id_usuario_insercion', blank=True, null=True, on_delete=models.CASCADE, related_name='id_usuario_insercion')
    nombre = models.CharField(max_length=100, blank=True, null=True)
    estado = models.BooleanField(blank=True, null=True)
    valor = models.CharField(max_length=200, blank=True, null=True)
    id_usuario_modificacion = models.ForeignKey('Usuario', db_column='id_usuario_modificacion', blank=True, null=True, on_delete=models.CASCADE, related_name='id_usuario_modificacion')

    class Meta:
        db_table = 'parametro'


class Persona(models.Model):
    id_persona = models.BigIntegerField(primary_key=True)
    numero_documento = models.CharField(max_length=30)
    nombre = models.CharField(max_length=200, blank=True, null=True)
    apellido = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'persona'
        unique_together = (('id_persona', 'numero_documento'),)


class Prioridad(models.Model):
    id_prioridad = models.BigIntegerField(primary_key=True)
    condicion = models.CharField(max_length=100, blank=True, null=True)
    prioridad = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'prioridad'


class SalaAtencion(models.Model):
    id_sala = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'sala_atencion'


class Servicio(models.Model):
    id_servicio = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'servicio'


class TipoUsuario(models.Model):
    id_tipo_usu = models.BigIntegerField(primary_key=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'tipo_usuario'


class Turno(models.Model):
    id_turno = models.BigIntegerField(primary_key=True)
    nro_ticket = models.BigIntegerField()
    id_cliente = models.ForeignKey(Cliente, db_column='id_cliente', blank=True, null=True, on_delete=models.CASCADE)
    id_prioridad = models.ForeignKey(Prioridad, db_column='id_prioridad', blank=True, null=True, on_delete=models.CASCADE)
    id_servicio = models.ForeignKey(Servicio, db_column='id_servicio', blank=True, null=True, on_delete=models.CASCADE)
    id_sala = models.ForeignKey(SalaAtencion, db_column='id_sala', blank=True, null=True, on_delete=models.CASCADE)
    id_usuario = models.ForeignKey('Usuario', db_column='id_usuario', blank=True, null=True, on_delete=models.CASCADE)
    fecha_hora = models.DateField(blank=True, null=True)
    orden_llegada = models.BigIntegerField(blank=True, null=True)
    estado = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = 'turno'
        unique_together = (('id_turno', 'nro_ticket'),)


class Usuario(models.Model):
    id_usuario = models.BigIntegerField(primary_key=True)
    id_persona = models.ForeignKey(Persona, db_column='id_persona', blank=True, null=True, on_delete=models.CASCADE)
    login = models.CharField(max_length=4, blank=True, null=True)
    numero_documento = models.CharField(max_length=30, blank=True, null=True)
    id_tipo_usu = models.ForeignKey(TipoUsuario, db_column='id_tipo_usu', blank=True, null=True, on_delete=models.CASCADE)
    estado = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = 'usuario'