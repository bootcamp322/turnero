from django import forms

class cargoformulario(forms.Form):
    id_usuario = forms.CharField(max_length=20, required=True)
    numero_documento = forms.CharField(max_length=20, required=True)
    estado =  forms.BooleanField(required=False)
