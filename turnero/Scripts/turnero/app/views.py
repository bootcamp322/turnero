from django.shortcuts import render
from .cargoform import cargoformulario
from .models import Usuario
# Create your views here.

def inicio(request):
        form = cargoformulario(request.POST or None)
        if form.is_valid():
                form_data = form.cleaned_data
                print(form_data)
                aux1 = form_data.get("numero_de_documento")
                aux2 = form_data.get("estado")
                aux3 = form.data.get("id_usuario")
                obj = Usuario.objects.create(numero_documento=aux1,estado=aux2,id_usuario=aux3)
        context = {
                "titulo": 'Turnero',
                "elform": form
        }
        return render(request,"inicio.html",context)