from django.contrib import admin
from django.apps import apps

from .models import Cliente
from .models import ColaAtencionCabecera
from .models import ColaAtencionDetalle
from .models import Empresa
from .models import Parametro
from .models import Persona
from .models import Prioridad
from .models import SalaAtencion
from .models import Servicio
from .models import TipoUsuario
from .models import Turno
from .models import Usuario
# Register your models here.

admin.site.register(Cliente)
admin.site.register(ColaAtencionCabecera)
admin.site.register(ColaAtencionDetalle)
admin.site.register(Empresa)
admin.site.register(Parametro)
admin.site.register(Persona)
admin.site.register(Prioridad)
admin.site.register(SalaAtencion)
admin.site.register(Servicio)
admin.site.register(TipoUsuario)
admin.site.register(Turno)
admin.site.register(Usuario)